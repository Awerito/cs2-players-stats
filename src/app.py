from fastapi import FastAPI
from src.process_data import process_data

app = FastAPI()

@app.get('/')
async def root():
    data = process_data()
    return data
