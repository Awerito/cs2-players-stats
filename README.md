# CS2 Player Stats

Extract all players stats in the given `.dem` files, using [akiver/cs-demo-analyzer](https://github.com/akiver/cs-demo-analyzer)

## Requirements:
  * Binary from [akiver/cs-demo-analyzer](https://github.com/akiver/cs-demo-analyzer) in your system, and `$PATH`
  * Put your `.dem` files in the `demos/` directory

## Run:
  Install pip requirements
  ```sh
    pip install -r requirements.txt
  ```
  Run
  ```sh
    python main.py
  ```
